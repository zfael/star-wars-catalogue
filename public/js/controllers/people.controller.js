(function () {
  'use strict';

  angular
    .module('app')
    .controller('PeopleController', Controller);

  Controller.$inject = ['$q', 'PersonService', 'PlanetService'];
  function Controller($q, PersonService, PlanetService) {

    var vm = this;
    vm.people;
    vm.planets;
    vm.isReady = false;
    vm.loadingMore = false;
    vm.filter = {text: ''};

    vm.order;
    vm.orderByOptions = [
      {field: 'name', description: 'Name (asc)' },
      {field: '-name', description: 'Name (desc)' },
      {field: 'gender', description: 'Gender (asc)' },
      {field: '-gender', description: 'Gender (desc)' }
    ];

    vm.loadMore = loadMore;
    vm.getPlanet = getPlanet;

    activate();

    ////////////////

    function activate() {
      fetch().then(function() {
        vm.isReady = true;
      });
    }

    function fetch(url) {
      return PersonService.fetch(url)
        .then(handlePeopleFetch)
        .then(handlePlanetFetch);
    }

    function handlePeopleFetch(result) {

      var planetsPromise = $q.all(_.map(result.results, function(person) {
        return PlanetService.fetchThisPlanet(person.homeworld);
      }));

      if (!vm.people) vm.people = result;
      else {
        vm.people.next = result.next;
        _.forEach(result.results, function(result) {
          vm.people.results.push(result);
        });
      }

      return planetsPromise;          
    }

    function handlePlanetFetch(planets) {
      if (!vm.planets) vm.planets = planets;
      else {
        _.forEach(planets, function(planet) {
          vm.planets.push(planet);
        });
      }
    }

    function loadMore() {
      vm.loadingMore = true;
      fetch(vm.people.next)
        .then(function() {
          vm.loadingMore = false;
        });
    }

    function getPlanet(planet) {
      var foundPlanet = _.find(vm.planets, { url : planet});
      return _.get(foundPlanet, 'name', '');
    }
  }
})();