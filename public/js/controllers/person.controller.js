(function () {
    'use strict';

    angular
        .module('app')
        .controller('PersonController', Controller);

    Controller.$inject = ['$q', '$stateParams', '$anchorScroll', 'PlanetService', 'PersonService'];
    function Controller($q, $stateParams, $anchorScroll, PlanetService, PersonService) {

        var vm = this;

        vm.peopleFromPlanetReady = false;
        vm.person = {};
        vm.residents = [];

        vm.personClick = personClick;

        activate();

        ////////////////

        function activate() {
            if ($stateParams.person) {
                vm.person = $stateParams.person;

                fetchPeopleFromPlanet();
            }
        }

        function fetchPeopleFromPlanet() {
            vm.peopleFromPlanetReady = false;
            vm.residents = [];

            PlanetService.fetchThisPlanet(vm.person.homeworld)
                .then(function(planet) {
                    return $q.all(_.map(planet.residents, function(resident) {
                        return PersonService.fetchPerson(resident);
                    }));
                })
                .then(function(residents) {
                    vm.residents = residents;

                    vm.peopleFromPlanetReady = true;
                });
        }

        function personClick(person) {
            $anchorScroll('top');
            vm.person = person;
            fetchPeopleFromPlanet();
        }
    }
})();