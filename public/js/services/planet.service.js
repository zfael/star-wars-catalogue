(function () {
    'use strict';

    angular
        .module('app')
        .service('PlanetService', Service);

    Service.$inject = ['$http'];
    function Service($http) {
        this.fetchThisPlanet = fetchThisPlanet;

        ////////////////

        function fetchThisPlanet(planetUrl) {
            return $http
                .get(planetUrl)
                .then(success, failure);
        }

        function success(response) {
            return response.data;
        }

        function failure(err) {
            return err.data;
        }
    }
})();