(function () {
    'use strict';

    angular
        .module('app')
        .service('PersonService', Service);

    Service.$inject = ['$http', 'Endpoint'];
    function Service($http, endpoint) {
        this.fetch = fetch;
        this.fetchPerson = fetchPerson;

        ////////////////

        function fetch(targetUrl) {
            var url = targetUrl ? targetUrl : endpoint.baseURL + endpoint.people;
            return $http
                .get(url)
                .then(success, failure);
        }

        function fetchPerson(url) {;
            return $http
                .get(url)
                .then(success, failure);
        }

        function success(response) {
            return response.data;
        }

        function failure(err) {
            return err.data;
        }
    }
})();