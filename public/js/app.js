(function() {
    'use strict';

    angular.module('app', [
        'ngMaterial',
        'ui.router'
    ])
    .config(['$mdThemingProvider', '$stateProvider', '$urlRouterProvider',
        function($mdThemingProvider, $stateProvider, $urlRouterProvider) {
            $mdThemingProvider.theme('default')
                .dark()
                .primaryPalette('blue');

            var people = {
                name: 'people',
                url: '/people',
                templateUrl: 'templates/people.html',
                data: {
                    selectedItem: undefined
                },
                params: {}
            };
            
            var personDetail = {
                name: 'detail',
                url: '/people/detail',
                templateUrl: 'templates/person.html',
                data: {
                    selectedItem: 'detail'
                },
                params: {
                    person: null
                }
            };

            $urlRouterProvider.otherwise('/people');
            $stateProvider
                .state(people)
                .state(personDetail);
    }]);
})();