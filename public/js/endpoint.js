(function() {
    'use strict';

    angular.module('app')
        .constant('Endpoint', {
            baseURL: 'https://swapi.co/api',
            people: '/people',
            planets: '/planets'
        });
})();