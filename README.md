# Star Wars Catalogue - Web Application

## Tech Stack

* [Angularjs](https://angularjs.org/)
* [Angularjs Material](https://material.angularjs.org/latest/)
* [Lodash](https://lodash.com/)

## Requirements 

Node.js @ 6.0.0

## Building Steps

```
npm install
bower install
```

### To Run

```
npm start
```

Application will be running on port 5000

### To build

```
npm run build
```

That command will minify and uglify all javascript files.

### To Serve

There are many ways to serve that applicant, right now I have two suggestions on top of my head.

* Using a pipeline solution (jenkins, codeship, etc), I would build that applicant and then upload it to a aws s3 and serve it through a cloudfront.
* Another way would be, still using pipeline, ssh into the server where that applicant needs to exists and then build all stuff and serve that.