var gulp = require("gulp");
var uglify = require("gulp-uglify");
var minify = require("gulp-minify");
var pump = require("pump");

gulp.task("build", function(cb) {
  pump(
    [
      gulp.src("./public/js/**/*.js"),
      minify({
        ext: {
          src: ".js",
          min: ".js"
        },
        ignoreFiles: [".min.js", "-min.js"],
        noSource: true
      }),
      uglify(),
      gulp.dest("./public/dist/js")
    ],
    cb
  );
});